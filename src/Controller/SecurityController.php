<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="app_security_register")
     */
    public function register(Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $encoder): Response
    {
        $user = new User();
        $formRegister = $this->createForm(RegistrationType::class, $user);

        $formRegister->handleRequest($request);
        if($formRegister->isSubmitted() && $formRegister->isValid()){
            $hash = $encoder->hashPassword($user, $user->getPassword());
            $user->setPassword($hash);

            $user->setRoles(['ROLE_USER']);

            $manager->persist($user);
            $manager->flush();

            $this->addFlash("success", "Vous êtes inscrit, merci.");

        }

        return $this->render('security/register.html.twig', [
            'form_register' => $formRegister->createView(),
        ]);
    }


     /**
     * @Route("/login", name="app_security_login")
     */
    public function login(): Response{
        return $this->render('security/login.html.twig', []);
    }


     /**
     * @Route("/logout", name="app_security_logout")
     */
    public function logout(){
        
    }


}
